import Mongoose from 'mongoose';
import { getTimeStamp } from './../utilities/universal';

const Schema = Mongoose.Schema;

class CategoryClass {
  static findAll (page, limit) {
    return this.find()
      .skip((limit * page) - limit)
      .limit(limit);
  }

  static countAll () {
    return this.countDocuments();
  }

  static findRoots () {
    return this.find({ parent: null });
  }

  static findSub (id) {
    return this.find({ parent: id });
  }

  static add (payload) {
    return this(payload).save();
  }

  static createOrUpdateById (payload) {
    return this.findOneAndUpdate(
      { id: payload.id },
      payload,
      { upsert: true, new: true, runValidators: true }
    );
  }

  static remove (id) {
    this.findOne({ _id: id }).remove();
  }
}

const CategoryShcema = new Schema({
  id: { type: Number, required: true },
  name: { type: String, required: true },
  parentId: { type: Number, required: true },
  createdAt: { type: Number, default: getTimeStamp },
  updatedAt: { type: Number, default: getTimeStamp }
});

CategoryShcema.loadClass(CategoryClass);

export default Mongoose.model('Category', CategoryShcema);
