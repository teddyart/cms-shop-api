import Mongoose from 'mongoose';
import { getTimeStamp } from './../utilities/universal';

const Schema = Mongoose.Schema;

class UserClass {
  static checkUserName (username) {
    return this.findOne({ username, isDeleted: false });
  }

  static checkEmail (email) {
    return this.findOne({ email, isDeleted: false });
  }

  static checkNumber (phone) {
    return this.findOne({ 'phone.number': phone ? phone.number : undefined, isDeleted: false });
  }

  static checkToken (token) {
    return this.findOne({ 'loginToken.token': token });
  }

  static register (payload) {
    return this(payload).save();
  }

  static login (uniqId, password, type = 0) {
    const role = type === 1004 ? 'admin' : 'user'; // todo magic number
    return this.findOne({
      $or: [{ username: uniqId }, { email: uniqId }, { 'phone.number': uniqId }],
      password,
      role,
      isDeleted: false
    });
  }

  static onLoginDone (userId, payload, loginToken) {
    const updateData = {
      $push: { loginToken: { token: loginToken } },
      $set: {
        lastLogin: getTimeStamp(),
        updateAt: getTimeStamp()
      }
    };
    return this.findByIdAndUpdate(userId, updateData, { new: true });
  }

  static logout (userId, token) {
    const updateData = {
      $set: {
        'device.token': '',
        updateAt: getTimeStamp()
      },
      $pull: { loginToken: { token: token } }
    };
    return this.findByIdAndUpdate(userId, updateData);
  }

  static findAll () {
    return this.find();
  }

  static update (payload) {
    return this.updateOne(
      { _id: payload.id },
      payload
    );
  }
}

const UserSchema = new Schema({
  username: { type: String, default: '' },
  email: { type: String },
  password: { type: String, required: true },
  phone: {
    code: { type: String, default: '' },
    number: { type: String, default: '' }
  },
  role: { type: String, required: true, default: 'user' },
  isDeleted: { type: Boolean, default: false },
  verified: {
    token: { type: String, default: '' },
    status: { type: Boolean, default: false }
  },
  loginToken: [
    {
      token: { type: String, default: '' },
      when: { type: Number, default: getTimeStamp }
    }
  ],
  lastLogin: { type: Number },
  isActive: { type: Boolean, default: true },
  createAt: { type: Number, default: getTimeStamp },
  updateAt: { type: Number, default: getTimeStamp }
});

UserSchema.loadClass(UserClass);

export default Mongoose.model('User', UserSchema);
