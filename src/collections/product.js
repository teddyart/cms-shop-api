import Mongoose from 'mongoose';
import { getTimeStamp } from './../utilities/universal';

const Schema = Mongoose.Schema;

class ProductClass {
  static findAll (page, limit, sort, order, filter) {
    const sortParams = {};
    sortParams[sort] = order;
    const findQuery = {};
    if (filter.vendor) {
      findQuery['vendor'] = { $regex: filter.vendor };
    }
    if (filter.spec_name) {
      findQuery['specName'] = { $regex: filter.spec_name };
    }
    if (filter.mrc) {
      findQuery['mrc'] = filter.mrc;
    }
    if (filter.mrc_from) {
      findQuery['mrc'] = { $gt: filter.mrc_from };
    }
    if (filter.mrc_to) {
      findQuery['mrc'] = { $lt: filter.mrc_to };
    }
    if (filter.packsize) {
      findQuery['packSize'] = filter.packsize;
    }

    return this.find(findQuery)
      .sort(sortParams)
      .skip((limit * page) - limit)
      .limit(limit);
  }

  static countAll (filter) {
    const findQuery = {};
    if (filter.vendor) {
      findQuery['vendor'] = { $regex: filter.vendor };
    }
    if (filter.spec_name) {
      findQuery['specName'] = { $regex: filter.spec_name };
    }
    if (filter.mrc) {
      findQuery['mrc'] = filter.mrc;
    }
    if (filter.mrc_from) {
      findQuery['mrc'] = { $gt: filter.mrc_from };
    }
    if (filter.mrc_to) {
      findQuery['mrc'] = { $lt: filter.mrc_to };
    }
    if (filter.packsize) {
      findQuery['packSize'] = filter.packsize;
    }
    return this.countDocuments(findQuery);
  }

  static findByCategory (categoryId) {
    return this.find({ category: categoryId });
  }

  static add (payload) {
    return this(payload).save();
  }

  static update (payload) {
    return this.updateOne(
      { _id: payload.id },
      payload
    );
  }

  static createOrUpdateByCode (payload) {
    return this.findOneAndUpdate(
      { code: payload.code },
      payload,
      { upsert: true, new: true, runValidators: true }
    );
  }

  static remove (id) {
    return this.deleteOne({ _id: id });
  }

  static get (id) {
    return this.findOne({ _id: id });
  }
}

const ProductSchema = new Schema({
  name: { type: String, required: true },
  categoryId: { type: String, required: true },
  kod: { type: Number, required: true },
  barcode: { type: Number, required: true },
  vendor: { type: String, required: true },
  model: { type: String, required: false },
  specName: { type: String, required: false },
  code: { type: String, required: true },
  vat: { type: Number, required: false },
  price: { type: Number, required: true },
  mrc: { type: Number, required: true },
  weight: { type: Number, required: true },
  packSize: { type: String, required: false },
  picture: { type: String, required: false },
  url: { type: String, required: false },
  quantum: { type: Number, required: true },
  createdAt: { type: Number, default: getTimeStamp },
  updatedAt: { type: Number, default: getTimeStamp }
});

ProductSchema.loadClass(ProductClass);

export default Mongoose.model('Product', ProductSchema);
