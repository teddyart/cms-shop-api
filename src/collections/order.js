import Mongoose from 'mongoose';

const Schema = Mongoose.Schema;

class OrderClass {
  static findAll (page, limit, sort, order, filter) {
    const sortParams = {};
    sortParams[sort] = order;

    // todo map filter
    return this.find(filter)
      .sort(sortParams)
      .skip((limit * page) - limit)
      .limit(limit);
  }

  static countAll (filter) {
    // const findQuery = {};
    // todo filter
    return this.countDocuments(filter);
  }

  static add (payload) {
    return this(payload).save();
  }

  static update (payload) {
    return this.updateOne(
      { _id: payload.id },
      payload
    );
  }

  static remove (id) {
    return this.deleteOne({ _id: id });
  }

  static get (id) {
    return this.findOne({ _id: id });
  }
}

const OrderItemSchema = new Schema({
  name: { type: String, required: true },
  product: { type: String, required: true },
  price: { type: Number, required: true },
  count: { type: Number, required: true }
});

const OrderDeliverySchema = new Schema({
  type: { type: String, required: true },
  address: { type: String, required: true },
  price: { type: Number, required: true }
});

const OrderPaymentSchema = new Schema({
  type: { type: String, required: true },
  currency: { type: String, required: true }
});

const OrderSchema = new Schema({
  number: { type: Number, required: true },
  user: { type: String, required: true },
  status: { type: String, required: true, default: 'HOLD' },
  items: { type: [OrderItemSchema], required: true },
  delivery: { type: OrderDeliverySchema, required: true },
  payment: { type: OrderPaymentSchema, required: true }
});

OrderSchema.loadClass(OrderClass);

export default Mongoose.model('Order', OrderSchema);
