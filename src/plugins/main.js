export default {
  name: 'Main',
  version: '1.0.0',
  register: (server, options) => {
    server.route({ path: '/{p*}', method: 'GET', handler: { file: './main.html' } });
  }
};
