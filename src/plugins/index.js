import Inert from '@hapi/inert';
import Vision from '@hapi/vision';
import HapiSwagger from 'hapi-swagger';
// import Good from '@hapi/good';
import config from 'config';
// ------------------------
import Auth from './auth';
import Rest from './rest';
import Main from './main';

export default [
  {
    plugin: Inert,
    options: {}
  },
  {
    plugin: Vision,
    options: {}
  },
  {
    plugin: HapiSwagger,
    options: {
      info: {
        title: 'API Documentation',
        version: config.get('app.version')
      },
      grouping: 'tags'
    }
  },
  // todo wainting types desc
  // {
  //   plugin: Good,
  //   options: {
  //     ops: {
  //       interval: 1000
  //     },
  //     reporters: {
  //       myConsoleReporter: [
  //         {
  //           module: 'good-squeeze',
  //           name: 'Squeeze',
  //           args: [{ log: '*', response: '*' }]
  //         },
  //         {
  //           module: 'good-console'
  //         },
  //         'stdout'
  //       ]
  //     }
  //   }
  // },
  {
    plugin: Auth,
    options: {}
  },
  {
    plugin: Rest,
    options: {}
  },
  {
    plugin: Main,
    options: {}
  }
];
