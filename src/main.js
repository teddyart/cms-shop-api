// eslint-disable-next-line no-unused-vars
import polyfill from '@babel/polyfill';
import configureDB from './db';
import configureServer from './server';
import configureJobs from './jobs/import';
import chalk from 'chalk';

const log = console.log;

// configurate Server
log(chalk.yellow('+++ Configurate Server'));
configureServer();
// create DB connection
log(chalk.yellow('+++ Configurate Data Base'));
configureDB();
// configure and run jobs
log(chalk.yellow('+++ Configurate Jobs'));
configureJobs();
