import Hapi from '@hapi/hapi';
import config from 'config';
// ---------------------------
import { failActionJoi } from './utilities/rest';
import logger from './utilities/logger';
import plugins from './plugins';
import chalk from 'chalk';

const app = config.get('app');

export default async () => {
  const server = new Hapi.Server({
    host: app.host,
    port: app.port,
    routes: {
      cors: {
        origin: ['*'],
        additionalHeaders: ['authorization'],
        additionalExposedHeaders: ['authorization']
      },
      validate: {
        failAction: failActionJoi
      }
    }
  });

  await server.register(plugins);

  try {
    await server.start();
    console.log(chalk.green(`+++ Server running at: ${server.info.uri}`));
  } catch (e) {
    logger.error(chalk.black.bgRed(`Error while starting server: ${e.message}`));
  }
};
