import schedule from 'node-schedule';
import chalk from 'chalk';
// ----------------------------------
import logger from '../utilities/logger';
import { getProductsFromRemoteXml } from '../services/synchronization';

const scheduleTime = { hour: 6, minute: 30 }; // every day at 6:30

export default async () => {
  try {
    schedule.scheduleJob(scheduleTime, () => {
      getProductsFromRemoteXml();
    });
    console.log(chalk.green('\t### Job Import Products...started'));
  } catch (e) {
    logger.error(`Error while starting Import Products Job: ${e.message}`);
    throw new Error('Job: Import of products failed');
  }
};
