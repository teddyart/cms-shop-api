import chalk from 'chalk';
// -------------------------------------
import logger from '../utilities/logger';
import importProduct from './importProduct';

const jobs = [
  importProduct
];

export default async () => {
  jobs.forEach(job => {
    try {
      job();
    } catch (e) {
      logger.error(chalk.black.bgRed(`Error while starting jobs: ${e.message}`));
    }
  });
  console.log(chalk.green('+++ All Jobs running'));
};
