import Joi from '@hapi/joi';
// --------------------------------------------------
import { removeCategory } from '../../../controllers/category';
import { checkAdminHandler } from './../../../utilities/rest';

export default {
  method: 'DELETE',
  path: '/api/v1/category/{id}',
  config: {
    auth: 'jwt',
    description: 'Api service used to remove category.',
    notes: '<br/>The request param must contain ID as unique identificator of category',
    tags: ['api', 'category'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      params: {
        id: Joi.string().trim().required().label('Id')
      },
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: removeCategory
};
