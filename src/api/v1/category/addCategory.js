import Joi from '@hapi/joi';
// -------------------------------------------------
import { addCategory } from '../../../controllers/category';
import { checkAdminHandler } from './../../../utilities/rest';

export default {
  method: 'POST',
  path: '/api/v1/category',
  config: {
    auth: 'jwt',
    description: 'Api service used to add new category.',
    notes: `<br/>The request object should contain following fields in its <b>Payload/Body</b> object
    <br/>&bull;<b> Name</b>: Should carry the Name of the category. There will bee 30 characters and with only lowercase & uppercase alphabets and numeric. This is a required field.`,
    tags: ['api', 'category'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      payload: Joi.object({
        name: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_ ]{1,30})$/).options({
          language: {
            string: {
              regex: {
                base: 'must be alphanumeric with 1 and 30 as min & max characters respectively'
              }
            }
          }
        }).label('Name'),
        parent: Joi.string().trim()
      }).label('Category'),
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: addCategory
};
