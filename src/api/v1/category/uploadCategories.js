import Joi from '@hapi/joi';
// --------------------------------------------------
import { uploadCategories } from '../../../controllers/category';
import { checkAdminHandler } from '../../../utilities/rest';

export default {
  method: 'POST',
  path: '/api/v1/categories/upload',
  config: {
    auth: 'jwt',
    description: 'Api service used to import categories',
    notes: '<br/>The request',
    tags: ['api', 'category'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: uploadCategories
};
