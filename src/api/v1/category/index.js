import addCategory from './addCategory';
import removeCategory from './removeCategory';
import uploadCategories from './uploadCategories';
import { findCategoriesRoute, findRootsCategoriesRoute, findSubCategoriesRoute } from './findCategories';

export default [
  findCategoriesRoute,
  findRootsCategoriesRoute,
  findSubCategoriesRoute,
  addCategory,
  removeCategory,
  uploadCategories
];
