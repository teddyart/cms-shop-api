import Joi from '@hapi/joi';
// -----------------------------------------
import { findCategories, findRootsCategories, findSubCategories } from '../../../controllers/category';

export const findCategoriesRoute = {
  method: 'GET',
  path: '/api/v1/categories',
  config: {
    description: 'Api service used to find all categories.',
    notes: '<br/>Get all categories',
    tags: ['api', 'category']
  },
  handler: findCategories
};

export const findRootsCategoriesRoute = {
  method: 'GET',
  path: '/api/v1/categories/roots',
  config: {
    description: 'Api service used to find all roots categories.',
    notes: '<br/>Get all roots categories. Roots mean that the category has no parent.',
    tags: ['api', 'category']
  },
  handler: findRootsCategories
};

export const findSubCategoriesRoute = {
  method: 'GET',
  path: '/api/v1/categories/{id}/sub',
  config: {
    description: 'Api service used to find categories which are subcategory.',
    notes: '<br/>Get subcategories. A subcategory means that the category has a parent whose ID is present in the parameters.',
    tags: ['api', 'category'],
    validate: {
      params: {
        id: Joi.string().trim().required().label('Id')
      }
    }
  },
  handler: findSubCategories
};
