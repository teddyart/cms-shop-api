import Joi from '@hapi/joi';
// ------------------------------------
import { addOrder } from '../../../controllers/order';
import {
  DELIVERY_TYPES,
  PAYMENTS_TYPES
} from '../../../utilities/const';

export default {
  method: 'POST',
  path: '/api/v1/order',
  config: {
    auth: 'jwt',
    description: 'Api для добавление заказа',
    notes: '<br/>Запрос должен содержать Body в формате JSON со следующими полями:',
    tags: ['api', 'order'],
    validate: {
      payload: Joi.object({
        id: Joi.string().allow(null).label('Id'),
        items: Joi.array().items(
          Joi.object({
            name: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
              language: {
                string: {
                  regex: {
                    base: 'неверный формат'
                  }
                }
              }
            }).label('Наименование позиции'),
            product: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
              language: {
                string: {
                  regex: {
                    base: 'неверный формат'
                  }
                }
              }
            }).label('Идентификатор позиции'),
            price: Joi.number().positive().required().options({
              language: {
                numbner: {
                  base: 'должно быть целое число, > 0'
                }
              }
            }).label('Цена'),
            count: Joi.number().positive().required().options({
              language: {
                number: {
                  base: 'должно быть целое число, > 0'
                }
              }
            }).label('Количество')
          })
        ),
        delivery: Joi.object({
          type: Joi.string().trim()
            .valid(DELIVERY_TYPES.pickup, DELIVERY_TYPES.fast, DELIVERY_TYPES.courie)
            .required(),
          address: Joi.string().trim().required()
        }).required().label('Delivery'),
        payment: Joi.object({
          type: Joi.string().trim()
            .valid(PAYMENTS_TYPES.card, PAYMENTS_TYPES.card_courie, PAYMENTS_TYPES.cash)
            .required()
        }).required().label('Payment')
      }).label('Заказ'),
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: addOrder
};
