import Joi from '@hapi/joi';
// --------------------------------------------------
import { removeOrder } from '../../../controllers/order';
import { checkAdminHandler } from '../../../utilities/rest';

export default {
  method: 'DELETE',
  path: '/api/v1/order/{id}',
  config: {
    auth: 'jwt',
    description: 'Удаление заказа',
    notes: '<br/>Параметр запроса должен содержать уникальный идентификатор заказа',
    tags: ['api', 'order'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      params: {
        id: Joi.string().trim().required().label('Id')
      },
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: removeOrder
};
