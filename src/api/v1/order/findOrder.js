import Joi from '@hapi/joi';
// -------------------------
import { findOrders, findMyOrders } from '../../../controllers/order';
import { checkAdminHandler } from '../../../utilities/rest';

export const findOrdersRoute = {
  method: 'GET',
  path: '/api/v1/orders',
  config: {
    description: 'Получение всех заказов',
    notes: '<br/>Получение всех заказов',
    auth: 'jwt',
    tags: ['api', 'order'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      query: Joi.object().required().keys({
        _limit: Joi.number().required(),
        _page: Joi.number().required(),
        _sort: Joi.string().optional(),
        _order: Joi.string().optional()
      }),
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: findOrders
};

export const findMyOrdersRoute = {
  method: 'GET',
  path: '/api/v1/my-orders',
  config: {
    description: 'Получение всех заказов текущего пользователя',
    notes: '<br/>Получение всех заказов текущего пользователя',
    auth: 'jwt',
    tags: ['api', 'order'],
    validate: {
      query: Joi.object().required().keys({
        _limit: Joi.number().required(),
        _page: Joi.number().required(),
        _sort: Joi.string().optional(),
        _order: Joi.string().optional()
      }),
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: findMyOrders
};
