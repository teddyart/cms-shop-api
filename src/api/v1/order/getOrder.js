import Joi from '@hapi/joi';
// -------------------------------------------------
import { getOrder } from '../../../controllers/order';

export default {
  method: 'GET',
  path: '/api/v1/order/{id}',
  config: {
    description: 'Получение заказа',
    notes: '<br/>Параметр запроса должен содержать уникальный идентификатор заказа',
    auth: 'jwt',
    tags: ['api', 'order'],
    validate: {
      params: {
        id: Joi.string().trim().required().label('Id')
      }
    }
  },
  handler: getOrder
};
