import addOrder from './addOrder';
import getOrder from './getOrder';
import removeOrder from './removeOrder';
import { findOrdersRoute, findMyOrdersRoute } from './findOrder';

export default [
  addOrder, getOrder, removeOrder,
  findOrdersRoute, findMyOrdersRoute
];
