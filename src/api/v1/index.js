import user from './user';
import category from './category';
import product from './product';
import order from './order';

export default [...user, ...category, ...product, ...order];
