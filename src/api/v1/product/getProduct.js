import Joi from '@hapi/joi';
// -------------------------------------------------
import { getProduct } from '../../../controllers/product';

export default {
  method: 'GET',
  path: '/api/v1/product/{id}',
  config: {
    description: 'Api service used to get product.',
    notes: '<br/>The request param must contain ID as unique identificator of product',
    tags: ['api', 'product'],
    validate: {
      params: {
        id: Joi.string().trim().required().label('Id')
      }
    }
  },
  handler: getProduct
};
