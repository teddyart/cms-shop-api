import Joi from '@hapi/joi';
// -------------------------------------------------
import { addProduct } from '../../../controllers/product';
import { checkAdminHandler } from '../../../utilities/rest';

export default {
  method: 'POST',
  path: '/api/v1/product',
  config: {
    auth: 'jwt',
    description: 'Api для добавления продукта.',
    notes: `<br/>Запрос должен содержать Body в формате JSON со следующими полями: 
            <br/>&bull;<b> name</b>:
            <br/>&bull;<b> kod</b>:
            <br/>&bull;<b> barcode</b>:
            <br/>&bull;<b> vendor</b>:
            <br/>&bull;<b> model</b>:
            <br/>&bull;<b> specName</b>:
            <br/>&bull;<b> code</b>:
            <br/>&bull;<b> vat</b>:
            <br/>&bull;<b> price</b>:
            <br/>&bull;<b> mrc</b>:
            <br/>&bull;<b> quantum</b>:`,
    tags: ['api', 'product'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      payload: Joi.object({
        id: Joi.string().allow(null).label('Id'),
        name: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
          language: {
            string: {
              regex: {
                base: 'Наименование не соотвествует формату'
              }
            }
          }
        }).label('Наименование'),
        categoryId: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
          language: {
            string: {
              regex: {
                base: 'Категория не соотвествует формату'
              }
            }
          }
        }).label('Категория'),
        kod: Joi.number().positive().required().options({
          language: {
            number: {
              base: 'Код товара не соответсвует формату'
            }
          }
        }).label('Код товара'),
        barcode: Joi.number().positive().required().options({
          language: {
            number: {
              base: 'Штрихкод не соответсвует формату'
            }
          }
        }).label('Штрихкод'),
        vendor: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
          language: {
            string: {
              regex: {
                base: 'Производитель не соотвествует формату'
              }
            }
          }
        }).label('Производитель'),
        model: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,100})$/).options({
          language: {
            string: {
              regex: {
                base: 'Модель не соотвествует формату'
              }
            }
          }
        }).label('Модель'),
        specName: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,30})$/).options({
          language: {
            string: {
              regex: {
                base: 'Наименование расцветки не соотвествует формату'
              }
            }
          }
        }).label('Наименование расцветки'),
        code: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,50})$/).options({
          language: {
            string: {
              regex: {
                base: 'Код товара + расцветка не соотвествует формату'
              }
            }
          }
        }).label('Код товара + расцветка'),
        vat: Joi.number().positive().allow(null).options({
          language: {
            number: {
              base: 'Vat не соответсвует формату'
            }
          }
        }).label('Vat'),
        price: Joi.number().positive().precision(2).required().options({
          language: {
            number: {
              base: 'Цена не соответсвует формату'
            }
          }
        }).label('Цена'),
        mrc: Joi.number().positive().required().options({
          language: {
            number: {
              base: 'Минимальная розничная цена не соответсвует формату'
            }
          }
        }).label('Минимальная розничная цена'),
        weight: Joi.number().positive().required().options({
          language: {
            number: {
              base: 'Вес не соответсвует формату'
            }
          }
        }).label('Вес'),
        packSize: Joi.string().allow(null).regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,50})$/).options({
          language: {
            string: {
              regex: {
                base: 'Размер не соотвествует формату'
              }
            }
          }
        }).label('Размер'),
        picture: Joi.string().trim().required().regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
          language: {
            string: {
              regex: {
                base: 'Изображение не соотвествует формату'
              }
            }
          }
        }).label('Изображение'),
        url: Joi.string().allow(null).regex(/^([А-Яа-яЕЁеёa-zA-Z0-9_\- \][.,:\\/"']{1,250})$/).options({
          language: {
            string: {
              regex: {
                base: 'Источник не соотвествует формату'
              }
            }
          }
        }).label('Источник'),
        quantum: Joi.number().positive().required().options({
          language: {
            number: {
              base: 'Остаток не соответсвует формату'
            }
          }
        }).label('Остаток')
      }).label('Продукт'),
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: addProduct
};
