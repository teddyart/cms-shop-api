import Joi from '@hapi/joi';
// --------------------------------------------------
import { uploadProducts } from '../../../controllers/product';
import { checkAdminHandler } from '../../../utilities/rest';

export default {
  method: 'POST',
  path: '/api/v1/products/upload',
  config: {
    auth: 'jwt',
    description: 'Api service used to export product',
    notes: '<br/>The request',
    tags: ['api', 'product'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: uploadProducts
};
