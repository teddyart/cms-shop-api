import addProduct from './addProduct';
import removeProduct from './removeProduct';
import getProduct from './getProduct';
import uploadProduct from './uploadProducts';
import { findProductsRoute, findProductsByCategoryRoute } from './findProduct';

export default [
  addProduct,
  removeProduct,
  getProduct,
  findProductsRoute,
  findProductsByCategoryRoute,
  uploadProduct
];
