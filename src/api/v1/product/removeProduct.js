import Joi from '@hapi/joi';
// --------------------------------------------------
import { removeProduct } from '../../../controllers/product';
import { checkAdminHandler } from '../../../utilities/rest';

export default {
  method: 'DELETE',
  path: '/api/v1/product/{id}',
  config: {
    auth: 'jwt',
    description: 'Api service used to remove product.',
    notes: '<br/>The request param must contain ID as unique identificator of product',
    tags: ['api', 'product'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      params: {
        id: Joi.string().trim().required().label('Id')
      },
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: removeProduct
};
