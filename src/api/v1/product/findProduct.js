import Joi from '@hapi/joi';
// -------------------------
import { findProducts, findProductsByCategory } from '../../../controllers/product';

export const findProductsRoute = {
  method: 'GET',
  path: '/api/v1/products',
  config: {
    description: 'Api service used to find all products.',
    notes: '<br/>Get all products',
    tags: ['api', 'product'],
    validate: {
      query: Joi.object().required().keys({
        _limit: Joi.number().required(),
        _page: Joi.number().required(),
        _sort: Joi.string().optional(),
        _order: Joi.string().optional(),
        vendor: Joi.string().optional(),
        spec_name: Joi.string().optional(),
        mrc: Joi.number().optional(),
        mrc_from: Joi.number().optional(),
        mrc_to: Joi.number().optional(),
        packsize: Joi.number().optional()
      })
    }
  },
  handler: findProducts
};

export const findProductsByCategoryRoute = {
  method: 'GET',
  path: '/api/v1/products/{categoryId}',
  config: {
    description: 'Api service used to find products by category.',
    notes: '<br/>Get products by category.',
    tags: ['api', 'product'],
    validate: {
      params: {
        categoryId: Joi.string().trim().required().label('Category ID')
      }
    }
  },
  handler: findProductsByCategory
};
