import register from './register';
import login from './login';
import logout from './logout';
import user from './user';
import updateUser from './updateUser';

export default [register, login, logout, user, updateUser];
