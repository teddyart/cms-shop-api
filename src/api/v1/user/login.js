import Joi from '@hapi/joi';
// --------------------------------------
import { loginUser } from './../../../controllers/user';
import { failActionJoi } from '../../../utilities/rest';

export default {
  method: 'POST',
  path: '/api/v1/user/login',
  config: {
    auth: false,
    description: 'Api service used to login user.',
    notes: `<br/>The request object should contain following fields in its <b>Payload/Body</b> object
    <br/>&bull;<b> Email</b>: Should be a valid email or valid phone number (10 digit with max 4 digit country code). 
    <br/>&bull;<b> Password</b>: Containing atleast one alphabet and one number, 6 - 8 characters.`,
    tags: ['api', 'user'],
    validate: {
      payload: Joi.object({
        email: Joi.string().trim().lowercase().email().required()
          .label('Email'),
        password: Joi.string().trim().required().regex(/^([a-zA-Z0-9_-]){6,8}$/)
          .options({
            language: {
              string: {
                regex: {
                  base: 'must be alphanumeric with 6 and 8 as min & max characters respectively'
                }
              }
            }
          }).label('Password')
      }).label('Login'),
      failAction: failActionJoi
    }
  },
  handler: loginUser
};
