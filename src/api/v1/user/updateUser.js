import Joi from '@hapi/joi';
// ------------------------------------
import { updateUser } from '../../../controllers/user';

export default {
  method: 'POST',
  path: '/api/v1/user',
  config: {
    auth: 'jwt',
    description: 'Api для обновления информации по пользователю',
    notes: '<br/>Запрос должен содержать Body в формате JSON со следующими полями:',
    tags: ['api', 'user'],
    validate: {
      payload: Joi.object({
        id: Joi.string().trim().required().label('Id'),
        username: Joi.string().trim().regex(/^([a-zA-Z_ ]){1,20}$/).options({
          language: {
            string: {
              regex: {
                base: 'should be valid name with maximum 20 characters and with only lowercase & uppercase alphabets, no numeric.'
              }
            }
          }
        }).label('User Name'),
        email: Joi.string().email().trim().lowercase().label('Email'),
        phone: Joi.object()
          .keys({
            code: Joi.string()
              .regex(/(\+\d{1,4})$/)
              .options({
                language: {
                  string: {
                    regex: {
                      base:
                        'should be valid, country code following a "+" character wih only 4 digit'
                    }
                  }
                }
              })
              .label('Country Code'),
            number: Joi.string()
              .regex(/^(1?(-?\d{3})-?)?(\d{3})(-?\d{4})$/)
              .options({
                language: {
                  string: {
                    regex: { base: 'should be valid phone number of max 10 characters' }
                  }
                }
              })
              .label('Phone Number')
          })
          .label('Contact Number'),
        password: Joi.string().trim().required().regex(/^([a-zA-Z0-9_ ]{6,8})$/).options({
          language: {
            string: {
              regex: {
                base: 'must be alphanumeric with 6 and 8 as min & max characters respectively'
              }
            }
          }
        }).label('Password')
      }).or('username', 'email', 'phone').label('User'),
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: updateUser
};
