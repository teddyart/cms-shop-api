import Joi from '@hapi/joi';
// -----------------------------------------
import { findAllUsers } from './../../../controllers/user';
import { failActionJoi, checkAdminHandler } from '../../../utilities/rest';

export default {
  method: 'GET',
  path: '/api/v1/users',
  config: {
    auth: 'jwt',
    description: 'Api service used to find all users.',
    tags: ['api', 'user'],
    pre: [{ method: checkAdminHandler }],
    validate: {
      failAction: failActionJoi,
      headers: Joi.object({
        authorization: Joi.string().required()
      }).unknown()
    }
  },
  handler: findAllUsers
};
