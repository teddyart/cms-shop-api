import axios from 'axios';
import xml2js from 'xml2js';
// -------------------------------
import { mapProductFromXml } from './../mappers/product';
import { mapCategoryFromXml } from './../mappers/category';
import Product from './../collections/product';
import Category from './../collections/category';

const KID_MASTER_URL = 'https://www.kidmaster.ru/remains.xml?uid=74478-90933&&fullinfo';

export const getProductsFromRemoteXml = async () => {
  console.log('123123123');
  try {
    const response = await axios.get(KID_MASTER_URL);
    const result = await xml2js.parseStringPromise(response.data);
    if (result.yml_catalog) {
      const offers = result.yml_catalog.shop[0].offers[0].offer;
      for (let i = 0; i < offers.length; ++i) {
        // todo запрос по ссылке не получается отфильтровать на >=, только на =
        // todo поэтому пока что так
        if (parseInt(offers[i].quantum[0]) < 5 || parseInt(offers[i].mrc[0]) < 1000) {
          continue;
        }
        let product = mapProductFromXml(offers[i]);
        product = await Product.createOrUpdateByCode(product);
      }

      return true;
    }
    throw Error('Данные не были получены');
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getCategoriesFromRemoteXml = async () => {
  try {
    const response = await axios.get(KID_MASTER_URL);
    const result = await xml2js.parseStringPromise(response.data);
    if (result.yml_catalog) {
      const categories = result.yml_catalog.shop[0].categories[0].category;
      for (let i = 0; i < categories.length; ++i) {
        let category = mapCategoryFromXml(categories[i]);
        category = await Category.createOrUpdateById(category);
      }

      return true;
    }
    throw Error('Данные не были получены');
  } catch (error) {
    console.log(error);
    return false;
  }
};
