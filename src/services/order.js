import Order from './../collections/order';
import { mapOrderFull } from './../mappers/order';

export const findAll = async (page, limit, sort, order, filter) => {
  const orders = await Order.findAll(page, limit, sort, order, filter);
  return orders.map(mapOrderFull);
};

export const countAll = async (filter) => {
  const count = await Order.countAll(filter);
  return count;
};

export const add = async payload => {
  const number = await Order.countAll();
  payload.number = number + 1;
  payload.delivery.price = 150; // todo delivery price is unkown yet
  payload.payment.currency = 'RUB'; // todo default currency is RUB
  const order = await Order.add(payload);
  return mapOrderFull(order);
};

export const update = async payload => {
  const order = await Order.update(payload);
  return mapOrderFull(order);
};

export const remove = async id => {
  const data = await Order.remove(id);
  return data;
};

export const get = async id => {
  const order = await Order.get(id);
  return mapOrderFull(order);
};
