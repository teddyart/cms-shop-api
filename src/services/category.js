import Category from './../collections/category';
import { getCategoriesFromRemoteXml } from './synchronization';

const mapCategoryFull = category => {
  return {
    id: category.id,
    name: category.name,
    parentId: category.parentId
  };
};

export const findAll = async (page, limit) => {
  const categories = await Category.findAll(page, limit);
  return categories.map(mapCategoryFull);
};

export const countAll = async () => {
  const count = await Category.countAll();
  return count;
};

export const findRoots = async () => {
  const categories = await Category.findRoots();
  return categories.map(mapCategoryFull);
};

export const findSub = async id => {
  const categories = await Category.findSub(id);
  return categories.map(mapCategoryFull);
};

export const add = async payload => {
  const category = await Category.add(payload);
  return mapCategoryFull(category);
};

export const remove = async id => {
  const data = await Category.remove(id);
  return data;
};

export const upload = async () => {
  try {
    const categories = await getCategoriesFromRemoteXml();
    return categories;
  } catch (e) {
    console.log(e);
  }
};
