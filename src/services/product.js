import Product from './../collections/product';
import { getProductsFromRemoteXml } from './synchronization';
import { mapProductFull } from '../mappers/product';

export const findAll = async (page, limit, sort, order, filter) => {
  const products = await Product.findAll(page, limit, sort, order, filter);
  return products.map(mapProductFull);
};

export const countAll = async (filter) => {
  const count = await Product.countAll(filter);
  return count;
};

export const findByCategory = async categoryId => {
  const products = await Product.findByCategory(categoryId);
  return products.map(mapProductFull);
};

export const add = async payload => {
  const product = await Product.add(payload);
  return mapProductFull(product);
};

export const update = async payload => {
  const product = await Product.update(payload);
  return mapProductFull(product);
};

export const remove = async id => {
  const data = await Product.remove(id);
  return data;
};

export const get = async id => {
  const product = await Product.get(id);
  return mapProductFull(product);
};

export const upload = async () => {
  try {
    const products = await getProductsFromRemoteXml();
    return products;
  } catch (e) {
    console.log(e);
  }
};
