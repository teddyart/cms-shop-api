// --------------------------------------
import User from './../collections/user';
import Messages from './../utilities/messages';
import * as Universal from './../utilities/universal';

export const register = async payload => {
  if (await User.checkUserName(payload.username)) throw new Error(Messages.userNameAlreadyExists);
  if (await User.checkEmail(payload.email)) throw new Error(Messages.emailAlreadyExists);
  if (await User.checkNumber(payload.phone)) throw new Error(Messages.contactAlreadyExists);

  delete payload.type;

  payload = {
    ...payload,
    password: Universal.encryptpassword(payload.password),
    verified: { token: Universal.generateRandom() }
  };

  const data = await User.register(payload);

  return {
    id: data._id,
    username: data.username,
    email: data.email,
    phone: data.phone
  };
};

export const login = async payload => {
  const userData = await User.login(
    payload.email,
    Universal.encryptpassword(payload.password)
  );
  if (!userData) throw new Error(Messages.invalidCredentials);
  // if (!userData.verified.status) throw new Error(Messages.userNotVerified);

  const loginToken = Universal.generateToken({
    when: Universal.getTimeStamp(),
    lastLogin: userData.lastLogin,
    userId: userData._id,
    name: userData.username
  });

  const data = await User.onLoginDone(userData._id, payload, loginToken);

  return {
    token: data.loginToken[data.loginToken.length - 1].token
  };
};

export const logout = async payload => {
  return User.logout(payload.user._id, payload.token);
};

export const findAll = async () => {
  return User.findAll();
};

export const update = async (payload) => {
  payload = {
    id: payload.id,
    username: payload.username,
    email: payload.email,
    password: Universal.encryptpassword(payload.password),
    phone: payload.phone
  };
  const user = User.update(payload);
  return {
    id: user._id,
    username: user.username,
    email: user.email,
    phone: user.phone
  };
};
