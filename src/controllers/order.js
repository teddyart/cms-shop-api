import {
  findAll,
  countAll,
  add,
  update,
  remove,
  get
} from './../services/order';
import { failAction, successAction, successActionTable } from './../utilities/rest';

export const findOrders = async (request, h) => {
  const limit = parseInt(request.query._limit) || 10;
  const page = parseInt(request.query._page) || 1;
  const sort = request.query._sort || 'id';
  const order = request.query._order || 'ASC';
  const filter = {}; // todo filter
  try {
    const orders = await findAll(page, limit, sort, order, filter);
    const count = await countAll(filter);
    return successActionTable(orders, count);
  } catch (e) {
    failAction(e.message);
  }
};

export const findMyOrders = async (request, h) => {
  const limit = parseInt(request.query._limit) || 10;
  const page = parseInt(request.query._page) || 1;
  const sort = request.query._sort || 'id';
  const order = request.query._order || 'ASC';
  const { credentials: { user: { _id } } } = request.auth;
  const filter = { user: _id };
  try {
    const orders = await findAll(page, limit, sort, order, filter);
    const count = await countAll(filter);
    return successActionTable(orders, count);
  } catch (e) {
    failAction(e.message);
  }
};

export const addOrder = async (request, h) => {
  const { payload } = request;
  const { credentials: { user: { _id } } } = request.auth;
  payload.user = _id;
  try {
    let order = null;
    if (payload.id) {
      order = await update(payload);
    } else {
      order = await add(payload);
    }
    return successAction(order);
  } catch (e) {
    failAction(e.message);
  }
};

export const removeOrder = async (request, h) => {
  const id = request.params.id;
  try {
    const success = await remove(id);
    return successAction(success);
  } catch (e) {
    failAction(e.massage);
  }
};

export const getOrder = async (request, h) => {
  const id = request.params.id;
  try {
    const order = await get(id);
    return order;
  } catch (e) {
    failAction(e.message);
  }
};
