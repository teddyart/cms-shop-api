import {
  findAll,
  add,
  update,
  remove,
  get,
  findByCategory,
  upload,
  countAll
} from './../services/product';
import { failAction, successAction, successActionTable } from './../utilities/rest';

export const findProducts = async (request, h) => {
  const limit = parseInt(request.query._limit) || 10;
  const page = parseInt(request.query._page) || 1;
  const sort = request.query._sort || 'id';
  const order = request.query._order || 'ASC';
  const filter = {
    vendor: request.query.vendor,
    spec_name: request.query.spec_name,
    mrc: request.query.mrc,
    mrc_from: request.query.mrc_from,
    mrc_to: request.query.mrc_to,
    packsize: request.query.packsize
  };
  try {
    const products = await findAll(page, limit, sort, order, filter);
    const count = await countAll(filter);
    return successActionTable(products, count);
  } catch (e) {
    failAction(e.message);
  }
};

export const findProductsByCategory = async (request, h) => {
  const categoryId = request.params.categoryId;
  try {
    const products = await findByCategory(categoryId);
    return successAction(products);
  } catch (e) {
    failAction(e.message);
  }
};

export const addProduct = async (request, h) => {
  const { payload } = request;
  try {
    let product = null;
    if (payload.id) {
      product = await update(payload);
    } else {
      product = await add(payload);
    }
    return successAction(product);
  } catch (e) {
    failAction(e.message);
  }
};

export const removeProduct = async (request, h) => {
  const id = request.params.id;
  try {
    const success = await remove(id);
    return successAction(success);
  } catch (e) {
    failAction(e.massage);
  }
};

export const getProduct = async (request, h) => {
  const id = request.params.id;
  try {
    const product = await get(id);
    return product;
  } catch (e) {
    failAction(e.message);
  }
};

export const uploadProducts = async (request, h) => {
  try {
    const result = await upload();
    return successAction(result);
  } catch (e) {
    failAction(e.message);
  }
};
