import { findAll, add, remove, findRoots, findSub, upload, countAll } from './../services/category';
import { failAction, successAction, successActionTable } from './../utilities/rest';
import { streamToPromise } from './../utilities/universal';

export const findCategories = async (request, h) => {
  const limit = parseInt(request.query._limit) || 10;
  const page = parseInt(request.query._page) || 1;
  try {
    const categories = await findAll(page, limit);
    const count = await countAll();
    return successActionTable(categories, count);
  } catch (e) {
    failAction(e.message);
  }
};

export const findRootsCategories = async (request, h) => {
  try {
    const categories = await findRoots();
    return successAction(categories);
  } catch (e) {
    failAction(e.message);
  }
};

export const findSubCategories = async (request, h) => {
  const id = request.params.id;
  try {
    const categories = await findSub(id);
    return successAction(categories);
  } catch (e) {
    failAction(e.message);
  }
};

export const addCategory = async (request, h) => {
  const { payload } = request;
  try {
    let data = null;
    if (payload.img) {
      data = await streamToPromise(payload.img);
    }
    const newCategory = await add({ ...payload, ...{ img: data } });
    return successAction(newCategory);
  } catch (e) {
    failAction(e.message);
  }
};

export const removeCategory = async (request, h) => {
  const id = request.params.id;
  try {
    const success = await remove(id);
    return successAction(success);
  } catch (e) {
    failAction(e.message);
  }
};

export const uploadCategories = async (request, h) => {
  try {
    const result = await upload();
    return successAction(result);
  } catch (e) {
    failAction(e.message);
  }
};
