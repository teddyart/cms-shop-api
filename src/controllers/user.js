import { register, login, logout, findAll, update } from './../services/user';
import { failAction, successAction } from './../utilities/rest';
import Messages from './../utilities/messages';

export const registerUser = async (request, h) => {
  const { payload } = request;
  try {
    const data = await register(payload);
    return successAction(data, Messages.registerSuccess);
  } catch (e) {
    failAction(e.message);
  }
};

export const loginUser = async (request, h) => {
  const { payload } = request;
  try {
    const data = await login(payload);
    return successAction(data, Messages.loginSuccessfull);
  } catch (e) {
    failAction(e.message);
  }
};

export const logoutUser = async (request, h) => {
  const { auth: { credentials: { user, token } } } = request;
  try {
    await logout({ user, token });
    return successAction(null, Messages.logoutSuccessfull);
  } catch (e) {
    failAction(e.message);
  }
};

export const findAllUsers = async (request, h) => {
  try {
    const users = await findAll();
    return successAction(users, Messages.registerSuccess);
  } catch (e) {
    failAction(e.message);
  }
};

export const updateUser = async (request, h) => {
  const { payload } = request;
  try {
    const user = await update(payload);
    return successAction(user);
  } catch (e) {
    failAction(e.message);
  }
};
