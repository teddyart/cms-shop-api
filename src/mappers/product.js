export const mapProductFromXml = (product) => {
  return {
    id: product.$.id,
    kod: parseInt(product.kod[0]),
    barcode: product.barcode[0].length <= 0 ? -1 : parseInt(product.barcode[0]),
    vendor: product.vendor[0],
    model: product.model ? product.model[0] : null,
    name: product.name[0],
    spec_name: product.spec_name ? product.spec_name[0]._ : null,
    code: product.code[0],
    vat: parseInt(product.vat[0]),
    categoryId: product.categoryId[0],
    weight: parseInt(product.weight[0]),
    packSize: product.packsize[0],
    picture: product.picture ? product.picture[0] : null,
    price: parseInt(product.price[0]),
    mrc: parseInt(product.mrc[0]),
    quantum: parseInt(product.quantum[0]),
    url: product.url[0]
  };
};

export const mapProductFull = product => {
  return {
    id: product._id,
    name: product.name,
    price: product.price,
    kod: product.kod,
    barcode: product.barcode,
    vendor: product.vendor,
    model: product.model,
    specName: product.specName,
    code: product.code,
    vat: product.vat,
    mrc: product.mrc,
    categoryId: product.categoryId,
    weight: product.weight,
    packSize: product.packSize,
    picture: product.picture,
    url: product.url,
    quantum: product.quantum
  };
};
