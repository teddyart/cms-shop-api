export const mapCategoryFromXml = (category) => {
  return {
    id: category.$.id,
    parentId: category.$.parentId,
    name: category._
  };
};
