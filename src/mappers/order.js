export const mapOrderFull = (order) => {
  return {
    id: order._id,
    number: order.number,
    items: order.items.map(i => {
      return {
        name: i.name,
        product: i.product,
        price: i.price,
        count: i.count
      };
    }),
    count: order.items.length
  };
};
