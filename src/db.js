import Mongoose from 'mongoose';
import Bluebird from 'bluebird';
import config from 'config';
import logger from './utilities/logger';
import chalk from 'chalk';

const db = config.get('db');

export default async () => {
  const url = db.auth
    ? `mongodb+srv://${db.username}:${db.password}@${db.host}/${db.name}`
    : `mongodb://${db.host}:${db.host}/${db.name}`;
  Mongoose.Promise = Bluebird;
  await Mongoose.connect(url, config.get('db.mongoose'), err => {
    if (err) {
      logger.error(chalk.black.bgRed('+++ DB Error: ' + err));
    } else {
      console.log(chalk.green('+++ DB connected'));
    }
  });
};
