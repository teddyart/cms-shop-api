export const DELIVERY_TYPES = {
  pickup: 'pickup',
  courie: 'courie',
  fast: 'fast'
};

export const PAYMENTS_TYPES = {
  card: 'card',
  cash: 'cash',
  card_courie: 'card_courie'
};
