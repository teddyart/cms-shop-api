import Boom from '@hapi/boom';
import jwt from 'jsonwebtoken';
import config from 'config';
// ---------------------------
import logger from './logger';
import Messages from './messages';
import User from './../collections/user';

export const failActionJoi = (request, h, error) => {
  let errorMessage = '';
  if (error.output.payload.message.indexOf('[') > -1) {
    errorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf('['));
  } else {
    errorMessage = error.output.payload.message;
  }
  errorMessage = errorMessage.replace(/"/g, '');
  errorMessage = errorMessage.replace('[', '');
  errorMessage = errorMessage.replace(']', '');
  error.output.payload.message = errorMessage;
  delete error.output.payload.validation;
  throw Boom.badRequest(errorMessage);
};

export const successAction = (data, message = 'OK') => ({
  statusCode: 200,
  message,
  data: data || undefined
});

export const successActionTable = (data, total, message = 'OK') => ({
  statusCode: 200,
  message,
  data: data || undefined,
  total: total || 0
});

export const failAction = errorMessage => {
  throw Boom.badRequest(errorMessage);
};

export const authorization = async (request, h) => {
  let token = request.headers['authorization'];
  if (token.includes('Bearer')) {
    token = token.replace('Bearer', '').trim();
  }
  let decoded = {};
  try {
    decoded = jwt.verify(token, config.app.jwtKey);
  } catch (err) {
    throw Boom.unauthorized(Messages.tokenExpired);
  }
  logger.info('authorization', decoded);
  const user = await User.checkToken(token);
  if (user) return h.authenticated({ credentials: { user, token } });
  else throw Boom.unauthorized(Messages.unauthorizedUser);
};

export const checkAdminHandler = async (request, h) => {
  let token = request.headers['authorization'];
  if (token.includes('Bearer')) {
    token = token.replace('Bearer', '').trim();
  }
  try {
    jwt.verify(token, config.app.jwtKey);
  } catch (err) {
    throw Boom.unauthorized(Messages.tokenExpired);
  }
  const user = await User.checkToken(token);
  if (user && user.role === 'admin') {
    return true;
  }
  throw Boom.unauthorized(Messages.unauthorizedUser);
};
