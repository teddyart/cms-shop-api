import Logger from 'bunyan';
import config from 'config';

export default new Logger({
  level: config.get('app.logLevel'),
  name: config.get('app.name'),
  serializers: Logger.stdSerializers
});
